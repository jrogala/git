package com.example.rgl95.git.mvvm;

import com.example.rgl95.git.data.api.ApiService;
import com.example.rgl95.git.data.models.Page;
import com.example.rgl95.git.data.models.Repo;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class GitRepository {

    private CompositeDisposable disposable;
    private ApiService apiService;

    private MutableLiveData<List<Repo>> repoLiveDataList = new MutableLiveData<>();
    private MutableLiveData<Page> pageLiveData = new MutableLiveData<>();

    public GitRepository(ApiService apiService, CompositeDisposable disposable) {
        this.apiService = apiService;
        this.disposable = disposable;
    }

    MutableLiveData<List<Repo>> getAllUserRepositories(String name) {
        disposable.add(apiService.getAllUserRepos(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        repo -> repoLiveDataList.setValue(repo),
                        Throwable::printStackTrace
                ));

        return repoLiveDataList;
    }

    MutableLiveData<Page> getListOfUsers(String name) {
        disposable.add(apiService.getListOfUsersWhoStartsWith(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        page -> pageLiveData.setValue(page),
                        Throwable::printStackTrace
                ));

        return pageLiveData;
    }
}
