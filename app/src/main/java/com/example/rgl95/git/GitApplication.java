package com.example.rgl95.git;

import android.app.Application;

import com.example.rgl95.git.di.AppComponent;
import com.example.rgl95.git.di.AppModule;
import com.example.rgl95.git.di.DaggerAppComponent;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

public class GitApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected @Nullable String createStackElementTag(@NotNull StackTraceElement element) {
                    return super.createStackElementTag(element) + " *** " + element.getClassName() + " " + element.getLineNumber();
                }
            });
        }

        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
