package com.example.rgl95.git.main;

import com.example.rgl95.git.data.models.Repo;
import com.example.rgl95.git.data.models.components.pageComponents.User;

import java.util.List;

public interface MainContract {

    interface View {

        void updateData(List<Repo> repo);

        void showInternetError();

        void setAutoComplete(List<String> userList);

        void setMaxBarValue(int limit);

        void setProgressBarValue(int remaining);

        void showProgressCircle();

        void showOnRequestError();

        void setMyAutoCompleteAdapter(List<User> items);
    }

    interface Presenter {

        void getUserByName(String name);

        void getListOfUsersWhoStartsWith(String name);

        void getRateLimit();
    }
}

