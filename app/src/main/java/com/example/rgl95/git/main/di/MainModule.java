package com.example.rgl95.git.main.di;

import android.net.ConnectivityManager;

import com.example.rgl95.git.data.api.ApiService;
import com.example.rgl95.git.main.MainContract;
import com.example.rgl95.git.main.presenter.MainPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class MainModule {

    private MainContract.View view;
//komentarbase
    public MainModule(MainContract.View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    MainContract.Presenter providesPresenter(ApiService apiService, CompositeDisposable disposable, ConnectivityManager connectivityManager) {
        return new MainPresenter(view, apiService, disposable, connectivityManager);
    }

}
