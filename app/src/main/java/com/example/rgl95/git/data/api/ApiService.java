package com.example.rgl95.git.data.api;

import com.example.rgl95.git.BuildConfig;
import com.example.rgl95.git.data.models.Page;
import com.example.rgl95.git.data.models.RateLimit;
import com.example.rgl95.git.data.models.Repo;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    String BASE_URL = BuildConfig.DEFAULT_BASE_URL;

    @GET("users/{name}/repos")
    Single<List<Repo>> getAllUserRepos(@Path("name") String userName);

    @GET("search/users")
    Single<Page> getListOfUsersWhoStartsWith(@Query("q") String userName);

    @GET("rate_limit")
    Single<RateLimit> getRateLimit();

}