package com.example.rgl95.git.data.models

data class Repo (val name: String, val description: String)