package com.example.rgl95.git.main.ui;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class MyObservableProvider {

    private AutoCompleteTextView typedName;
    private Button observableButton;

    public MyObservableProvider(AutoCompleteTextView typedName, Button observableButton) {
        this.typedName = typedName;
        this.observableButton = observableButton;
    }

    public Observable<String> createTextObservable() {

        Observable<String> textChangeObservable = Observable.create
                (
                        emitter -> {
                            TextWatcher watcher = new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    emitter.onNext(String.valueOf(s));
                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                }
                            };
                            typedName.addTextChangedListener(watcher);
                            emitter.setCancellable(() -> typedName.removeTextChangedListener(watcher));
                        }
                );


        return textChangeObservable
                .filter(s -> s.length() > 2)
                .debounce(300, TimeUnit.MILLISECONDS);
    }

    public Observable<String> createButtonClickObservable() {

        return Observable.create
                (
                        emitter -> {
                            observableButton.setOnClickListener(
                                    view -> emitter.onNext(typedName.getText().toString())
                            );

                            typedName.setOnItemClickListener((parent, view, position, id) -> emitter.onNext(typedName.getText().toString()));
                            emitter.setCancellable(() -> observableButton.setOnClickListener(null));
                        }
                );
    }

}
