package com.example.rgl95.git.data.models.components.pageComponents

import com.google.gson.annotations.SerializedName

data class User(val login: String, @SerializedName("avatar_url") val avatar: String)