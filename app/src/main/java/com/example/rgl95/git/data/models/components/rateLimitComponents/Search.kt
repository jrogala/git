package com.example.rgl95.git.data.models.components.rateLimitComponents

data class Search(val limit: Int, val remaining: Int)