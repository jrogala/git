package com.example.rgl95.git.mvvm;

import com.example.rgl95.git.data.models.Page;
import com.example.rgl95.git.data.models.Repo;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class GitViewModel extends ViewModel {

    private GitRepository repository;

    public void setRepository(GitRepository repository) {
        this.repository = repository;
    }

    LiveData<List<Repo>> getAllUserRepos(String name) {
        return repository.getAllUserRepositories(name);
    }

    LiveData<Page> getAllUsersNamesWith(String name) {
        return repository.getListOfUsers(name);
    }
}
