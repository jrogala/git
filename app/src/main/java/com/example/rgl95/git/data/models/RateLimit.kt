package com.example.rgl95.git.data.models

import com.example.rgl95.git.data.models.components.rateLimitComponents.Resources

data class RateLimit (val resources: Resources)