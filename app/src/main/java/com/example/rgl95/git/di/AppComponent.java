package com.example.rgl95.git.di;

import com.example.rgl95.git.ApplicationScope;
import com.example.rgl95.git.main.di.MainComponent;
import com.example.rgl95.git.main.di.MainModule;
import com.example.rgl95.git.mvvm.di.Main2Component;
import com.example.rgl95.git.mvvm.di.Main2Module;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {

    MainComponent plus(MainModule mainModule);

    Main2Component plus(Main2Module main2Module);
}
