package com.example.rgl95.git.main.ui;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rgl95.git.R;
import com.example.rgl95.git.data.models.components.pageComponents.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class AutoCompleteAdapter extends ArrayAdapter<User> {

    private List<User> userListFull;

    public AutoCompleteAdapter(@NonNull Context context, @NonNull List<User> userList) {
        super(context, 0, userList);
        userListFull = new ArrayList<>(userList);
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return userFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.autocomplete_item_list, parent, false);

        TextView textView = convertView.findViewById(R.id.text_view_list_item);
        ImageView imageView = convertView.findViewById(R.id.user_photo);

        User user = getItem(position);

        if(user != null){
            textView.setText(user.getLogin());
            Picasso.get().load(user.getAvatar()).into(imageView);
        }

        return convertView;
    }

    private Filter userFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults filterResults = new FilterResults();
            List<User> suggestions = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(userListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (User item : userListFull) {
                    if (Objects.requireNonNull(item.getLogin()).toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }
            filterResults.values = suggestions;
            filterResults.count = suggestions.size();

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();

            addAll((List) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((User) resultValue).getLogin();
        }
    };


}


/*
package com.example.rgl95.git.main.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rgl95.git.R;
import com.example.rgl95.git.data.models.components.pageComponents.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


class AutoCompleteAdapter extends ArrayAdapter<User> {

    private Context context;
    private int resource;
    private int textViewResourceId;
    private List<User> userList, filteredUsers, userListAll;

    AutoCompleteAdapter(Context context, int resource, int textViewResourceId, List<User> userList) {
        super(context, resource, textViewResourceId, userList);

        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.userList = userList;
        this.filteredUsers = new ArrayList<User>();
        this.userListAll = userList;
    }

    @Nullable
    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.autocomplete_item_list, parent, false);

            TextView textView = (TextView) view.findViewById(R.id.text_view_list_item);
            ImageView imageView = (ImageView) view.findViewById(R.id.user_photo);

            textView.setText(userList.get(position).getLogin());
            Picasso.get().load(userList.get(position).getAvatar()).into(imageView);

        }


        return view;
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    private Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            FilterResults filterResults = new FilterResults();

            if (constraint != null) {
                filteredUsers.clear();

                for (User user : userListAll) {
                    if (user.getLogin().contains(constraint))
                        filteredUsers.add(user);
                }

                filterResults.values = filteredUsers;
                filterResults.count = filteredUsers.size();
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            List<User> filteredUserList = (List<User>) results.values;

            if (results != null && results.count > 0) {
                clear();

                for (User user : filteredUserList) {
                    add(user);
                }
                notifyDataSetChanged();
            } else
                notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((User) resultValue).getLogin();
        }
    };
}

 */