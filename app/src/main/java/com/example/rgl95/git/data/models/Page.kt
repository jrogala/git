package com.example.rgl95.git.data.models

import com.example.rgl95.git.data.models.components.pageComponents.User

data class Page(val items: List<User>)