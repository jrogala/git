package com.example.rgl95.git.mvvm.di;

import com.example.rgl95.git.data.api.ApiService;
import com.example.rgl95.git.mvvm.GitRepository;
import com.example.rgl95.git.mvvm.GitViewModel;
import com.example.rgl95.git.mvvm.Main2Activity;

import javax.inject.Singleton;

import androidx.lifecycle.ViewModelProviders;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class Main2Module {

    private Main2Activity activity;

    public Main2Module(Main2Activity activity) {
        this.activity = activity;
    }

    @Provides
    @Singleton
    GitRepository providesGitRepository(ApiService apiService, CompositeDisposable disposable){
        return new GitRepository(apiService, disposable);
    }

    @Provides
    @Singleton
    GitViewModel providesGitViewModel(GitRepository repository){
        GitViewModel viewModel = ViewModelProviders.of(activity).get(GitViewModel.class);
        viewModel.setRepository(repository);
        return viewModel;

    }
}