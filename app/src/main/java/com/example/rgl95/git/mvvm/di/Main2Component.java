package com.example.rgl95.git.mvvm.di;

import com.example.rgl95.git.mvvm.Main2Activity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {Main2Module.class})
public interface Main2Component {

    void inject(Main2Activity activity);

}