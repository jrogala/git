package com.example.rgl95.git.main.ui;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rgl95.git.R;
import com.example.rgl95.git.data.models.Repo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<Repo> repoList = new ArrayList<>();

    public void updateData(List<Repo> repo) {
        repoList.clear();
        repoList.addAll(repo);
        notifyDataSetChanged();
        Timber.d("updateData() ADP");
    }

    @NonNull
    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_repo, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MainAdapter.ViewHolder holder, int position) {
        holder.setup(repoList.get(position));
    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.repo_name)
        TextView repoName;

        @BindView(R.id.repo_description)
        TextView repoDescription;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setup(Repo repo) {
            repoName.setText(repo.getName());
            repoDescription.setText(repo.getDescription());

            if (repo.getDescription() == null)
                repoDescription.setVisibility(View.GONE);
        }

    }

}