package com.example.rgl95.git.main.presenter;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.rgl95.git.data.api.ApiService;
import com.example.rgl95.git.data.models.components.pageComponents.User;
import com.example.rgl95.git.main.MainContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MainPresenter implements MainContract.Presenter, LifecycleObserver {

    private MainContract.View view;
    private ApiService apiService;
    private CompositeDisposable disposable;
    private ConnectivityManager connectivityManager;
    private List<String> suggestedUsersList = new ArrayList<>();

    public MainPresenter(MainContract.View view, ApiService apiService, CompositeDisposable disposable,
                         ConnectivityManager connectivityManager) {
        this.view = view;
        this.apiService = apiService;
        this.disposable = disposable;
        this.connectivityManager = connectivityManager;

        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
    }

    @Override
    public void getUserByName(String name) {
        view.showProgressCircle();

        if (!checkInternetConnect())
            view.showInternetError();

        disposable.add(apiService.getAllUserRepos(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        repo -> view.updateData(repo),
                        throwable -> {
                            view.showOnRequestError();
                            throwable.printStackTrace();
                        }
                ));

        Timber.d("getUserByName() PRE");
    }

    @Override
    public void getListOfUsersWhoStartsWith(String name) {
        disposable.add(apiService.getListOfUsersWhoStartsWith(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        page -> updateList(page.getItems()),
                        Throwable::printStackTrace
                ));
    }

    @Override
    public void getRateLimit() {
        disposable.add(apiService.getRateLimit()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        rateLimit -> {
                            view.setMaxBarValue(rateLimit.getResources().getSearch().getLimit());
                            view.setProgressBarValue(rateLimit.getResources().getSearch().getRemaining());
                        },
                        Throwable::printStackTrace
                ));
    }


    private void updateList(List<User> items) {
        suggestedUsersList.clear();

        for (int i = 0; i < items.size(); i++) {
            suggestedUsersList.add(items.get(i).getLogin());
        }

        Collections.sort(suggestedUsersList, String.CASE_INSENSITIVE_ORDER);

//        view.setAutoComplete(suggestedUsersList);
        view.setMyAutoCompleteAdapter(items);
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onActivityStop() {
        disposable.clear();
        Timber.d(String.valueOf(disposable.size()));
    }

    private boolean checkInternetConnect() {
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }
}
