package com.example.rgl95.git.main.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rgl95.git.GitApplication;
import com.example.rgl95.git.R;
import com.example.rgl95.git.data.models.Repo;
import com.example.rgl95.git.data.models.components.pageComponents.User;
import com.example.rgl95.git.error.ErrorActivity;
import com.example.rgl95.git.main.MainContract;
import com.example.rgl95.git.main.di.MainModule;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView recycler;

    @BindView(R.id.user_name_edit_text)
    AutoCompleteTextView typedName;

    @BindView(R.id.progress_bar)
    ProgressBar requestBar;

    @BindView(R.id.left_req_number)
    TextView leftRequests;

    @BindView(R.id.search_button)
    Button observableButton;

    @BindView(R.id.no_repo)
    TextView noRepoText;

    @BindView(R.id.progress_circle)
    ProgressBar progressCircle;


    @Inject
    MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ((GitApplication) getApplication())
                .getAppComponent()
                .plus(new MainModule(this))
                .inject(this);

        setupAdapter();

    }

    @SuppressLint("CheckResult")
    protected void onStart() {
        super.onStart();

        MyObservableProvider observableProvider = new MyObservableProvider(typedName, observableButton);

        Observable<String> buttonClickObservable = observableProvider.createButtonClickObservable();
        Observable<String> textChangeObservable = observableProvider.createTextObservable();

        buttonClickObservable.subscribe(text -> presenter.getUserByName(text));

        textChangeObservable.subscribe(text -> {
            presenter.getRateLimit();
            presenter.getListOfUsersWhoStartsWith(text);
        });
    }

    private void setupAdapter() {
        adapter = new MainAdapter();
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);
    }

    @Override
    public void setAutoComplete(List<String> userList) {

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.autocomplete_item_list, R.id.text_view_list_item, userList);

        typedName.setAdapter(arrayAdapter);
    }

    @Override
    public void setMyAutoCompleteAdapter(List<User> items) {

        AutoCompleteAdapter myAdapter = new AutoCompleteAdapter(this, items);

        typedName.setAdapter(myAdapter);
    }

    @Override
    public void updateData(List<Repo> repos) {
        isThereRepos(repos);
        adapter.updateData(repos);
    }

    @Override
    public void setProgressBarValue(int remaining) {
        requestBar.setProgress(remaining);
        leftRequests.setText(String.valueOf(remaining));
    }

    @Override
    public void showProgressCircle() {
        progressCircle.setVisibility(View.VISIBLE);
    }

    @Override
    public void showOnRequestError() {
        startActivity(new Intent(this, ErrorActivity.class));
        hideProgressCircle();
    }


    private void hideProgressCircle() {
        progressCircle.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showInternetError() {
        startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), 1);

        Toast.makeText(this, "Please, turn on your Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setMaxBarValue(int limit) {
        requestBar.setMax(limit);
    }

    private void isThereRepos(List<Repo> repos) {
        hideProgressCircle();
        if (repos.size() == 0)
            noRepoText.setVisibility(View.VISIBLE);
        else
            noRepoText.setVisibility(View.INVISIBLE);
    }
}