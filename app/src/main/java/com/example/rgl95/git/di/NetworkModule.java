package com.example.rgl95.git.di;

import com.example.rgl95.git.data.api.ApiService;
import com.example.rgl95.git.ApplicationScope;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @ApplicationScope
    Retrofit providesRetrofit() {
        return new Retrofit
                .Builder()
                .baseUrl(ApiService.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }

    @Provides
    @ApplicationScope
    ApiService providesApi(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

}
