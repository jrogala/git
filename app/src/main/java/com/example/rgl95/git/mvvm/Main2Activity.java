package com.example.rgl95.git.mvvm;

import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.rgl95.git.GitApplication;
import com.example.rgl95.git.R;
import com.example.rgl95.git.data.models.Repo;
import com.example.rgl95.git.data.models.components.pageComponents.User;
import com.example.rgl95.git.main.ui.AutoCompleteAdapter;
import com.example.rgl95.git.main.ui.MainAdapter;
import com.example.rgl95.git.main.ui.MyObservableProvider;
import com.example.rgl95.git.mvvm.di.Main2Module;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recycler;

    @BindView(R.id.user_name_edit_text)
    AutoCompleteTextView typedName;

    @BindView(R.id.progress_bar)
    ProgressBar requestBar;

    @BindView(R.id.left_req_number)
    TextView leftRequests;

    @BindView(R.id.no_repo)
    TextView noRepoText;

    @BindView(R.id.search_button)
    Button searchButton;

    @BindView(R.id.progress_circle)
    ProgressBar progressCircle;

    @Inject
    GitViewModel viewModel;

    MainAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ((GitApplication) getApplication())
                .getAppComponent()
                .plus(new Main2Module(this))
                .inject(this);

        setupAdapter();


    }

    protected void onStart() {
        super.onStart();

        MyObservableProvider observableProvider = new MyObservableProvider(typedName, searchButton);

        Observable<String> buttonClickObservable = observableProvider.createButtonClickObservable();
        Observable<String> textChangeObservable = observableProvider.createTextObservable();

        buttonClickObservable.subscribe(text -> viewModel.getAllUserRepos(text).observe(Main2Activity.this, repos -> {
            isThereRepos(repos);
        }));

        textChangeObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(name -> viewModel.getAllUsersNamesWith(name).observe(Main2Activity.this, page -> {
            setMyAutoCompleteAdapter(page.getItems());
        }));
    }


    private void setupAdapter() {
        adapter = new MainAdapter();
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);
    }

    public void setMyAutoCompleteAdapter(List<User> items) {

        AutoCompleteAdapter myAdapter = new AutoCompleteAdapter(this, items);

        typedName.setAdapter(myAdapter);
    }

    private void isThereRepos(List<Repo> repos) {
        if (repos.size() == 0)
            noRepoText.setVisibility(View.VISIBLE);
        else {
            noRepoText.setVisibility(View.INVISIBLE);
            adapter.updateData(repos);
        }
    }

}
